﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dyd.BaseService.Monitor.Core
{
    public class CoreGlobalConfig
    {
        public static string PlatformManageConnectString { get; set; }

        /// <summary>
        /// 从连接字符串中得到数据库名
        /// </summary>
        /// <param name="connstr"></param>
        /// <returns></returns>
        public static string GetConnStringDbName(string connstr)
        {
            string[] databasename = new string[] { "Initial Catalog".ToLower(), "Database".ToLower() };
            string[] arr = (connstr ?? "").Split(";".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = arr[i].Trim();
                bool ok = false;
                foreach (var a in databasename)
                {
                    if (arr[i].ToLower().StartsWith(a))
                    {
                        ok = true;
                        break;
                    }
                }
                if (ok)
                {
                    arr = arr[i].Split("=".ToArray(), StringSplitOptions.RemoveEmptyEntries);
                    return arr[1].Trim();
                }
            }
            return null;
        }
    }
}
