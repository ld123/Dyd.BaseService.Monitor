﻿using Dyd.BaseService.Monitor.Domain.PlatformManage.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using XXF.BaseService.Monitor.SystemRuntime;
using XXF.Db;

namespace Web.Models
{
    public class Config
    {
        private static string _ClusterConnectString = null;
        private static string _UnityLogConnectString = null;

        public static string PlatformManageConnectString = System.Configuration.ConfigurationManager.AppSettings["PlatformManageConnectString"].ToString();
        public static string ClusterConnectString
        {
            get
            {
                if (string.IsNullOrEmpty(_ClusterConnectString))
                {
                    InitConfig();
                }
                return _ClusterConnectString;
            }
        }
        public static string UnityLogConnectString
        {
            get
            {
                if (string.IsNullOrEmpty(_UnityLogConnectString))
                {
                    InitConfig();
                }
                return _UnityLogConnectString;
            }
        }

        private static void InitConfig()
        {
            try
            {
                Dyd.BaseService.Monitor.Domain.PlatformManage.Dal.tb_database_config_dal dal = new Dyd.BaseService.Monitor.Domain.PlatformManage.Dal.tb_database_config_dal();
                List<tb_database_config_model> databaseList = new List<tb_database_config_model>();
                using (DbConn PubConn = DbConfig.CreateConn(PlatformManageConnectString))
                {
                    PubConn.Open();
                    databaseList = dal.GetModelList(PubConn);
                }
                foreach (tb_database_config_model m in databaseList)
                {
                    string Connection = dal.ReplaceConnectStringTemplate(m);

                    if (m.dbtype == (int)DataBaseType.Cluster)
                    {
                        _ClusterConnectString = Connection;
                    }
                    if (m.dbtype == (int)DataBaseType.UnityLog)
                    {
                        _UnityLogConnectString = Connection;
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("读取库配置失败！" + ex.Message);
            }
        }
    }
}